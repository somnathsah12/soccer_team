<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Soccer Team</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="content">
            @forelse ($teamList as $team)
                <div class="form-group">
                    <div class="col-sm-1" onclick="showTeamDeatils({{ $team->team_id }})">
                        <input type="hidden" id="teamId" value="{{ $team->team_id}}">
                        <img src="{{ $team->logo_uri }}"><br>
                        <div class="text-center">{{ $team->name }}</div>
                    </div>
                </div>                    
            @empty
                <p>No Teams </p>
            @endforelse
            <div class="form-group">
                <div class="col-sm-12">
                    <div id="players">
                    </div>
                </div>
            </div>

        </div>
         <script type="text/javascript">
            function showTeamDeatils(teamId) {
                $.ajax({
                  url: "{{ url('show_team') }}",
                  data: {
                    teamId : teamId
                  },
                  success: function(html){
                    $("#players").empty().append(html);
                  }
                });
            }
         </script>
    </body>
</html>
