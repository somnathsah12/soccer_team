<div class="form-group">
	@forelse ($players as $player)
		<div class="col-sm-1">
			<img src="{{ url($player->logo_uri)}}" height="65px" width="65px">
			<div> {{ $player->name }} </div>
		</div>
    @empty
    	No players
    @endforelse
</div>