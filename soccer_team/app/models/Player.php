<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player';

    /**
     * Get the team of a player.
     */
    public function team()
    {
        return $this->belongsTo('App\models\Team', 'team_id', 'team_id');
    }
}

