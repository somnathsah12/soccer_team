<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'team';

    /**
     * Get the players of a team.
     */
    public function players()
    {
        return $this->hasMany('App\models\Player', 'team_id', 'team_id');
    }
}

