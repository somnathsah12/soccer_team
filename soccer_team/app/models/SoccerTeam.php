<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model,
    App\models\Team,
    App\models\Player;

class SoccerTeam extends Model
{
    /**
     * Return all the soccer team lists in the system
     * @return Team object
     */
    public function getAllTeams()
    {
        return Team::all();;
    }

    /**
     * Get the lists of all the players of a team
     * @param  int     $teamId
     * @return Players object
     */
    public function getTeamPlayers($teamId)
    {
        return Team::where('team_id', '=', $teamId)
            ->get()
            ->first()
            ->Players()
            ->get();
    }

    /**
     * Get the team details od a player
     * @param  int  $playerId
     * @return Team object
     */
    public function getPlayerTeam($playerId)
    {
        return Player::where('player_id', '=', $playerId)
            ->get()
            ->first()
            ->Team()
            ->get();
    }
}

