<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller,
    Illuminate\Http\Request,
    App\models\SoccerTeam,
    Validator;;

class HomeController extends Controller
{
	public function __construct()
	{
		$this->soccerTeam = new SoccerTeam;
	}

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        
        $teamList = $this->soccerTeam->getAllTeams();

        return view('soccerTeam', ['teamList' => $teamList]);
    }

    public function showTeam(Request $request)
    {
    	$validator = $this->Validate($request, [
    		'teamId' => 'integer',
		]);

        $players = $this->soccerTeam->getTeamPlayers($request->teamId);

        return view('players', ['players' => $players]);
    }
}